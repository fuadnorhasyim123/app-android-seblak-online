package com.example.appblak_on

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.strictmode.InstanceCountViolation
import android.view.AbsSavedState
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        var email = edUsername.text.toString()
        var password = edPassword.text.toString()

        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Username Atau Password Tidak Boleh Kosong", Toast.LENGTH_SHORT)
                .show()
            }else {
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Authenticating...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "Berhasil Mendaftar", Toast.LENGTH_SHORT).show()
                    var intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this, "", Toast.LENGTH_SHORT).show()
                }
        }
    }
    override fun onCreate(savedInstanceState : Bundle?) {
       super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnRegister.setOnClickListener(this)
    }

}