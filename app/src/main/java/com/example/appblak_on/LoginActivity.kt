package com.example.appblak_on

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin.setOnClickListener(this)
        btnSignUp.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> {
                var email = edUsername.text.toString()
                var password = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username Atau Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show()
                } else if (email == "admin" && password == "admin") {
                    Toast.makeText(this, "Berhasil Login Admin", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                } else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                                Toast.makeText(this, "Berhasil Login", Toast.LENGTH_SHORT).show()
                                val intent = Intent(this, MainActivity::class.java)
                                startActivity(intent)
                            }
                                .addOnFailureListener {
                                    progressDialog.hide()
                                    Toast.makeText(this, "Username Atau Password Salah", Toast.LENGTH_SHORT).show()
                                }
                        }
                }
                R.id.btnSignUp -> {
                    var intent = Intent(this, SignUpActivity::class.java)
                    startActivity(intent)
                }

            }
        }
    }

